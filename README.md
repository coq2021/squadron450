
## Development Environments



* Python 3.4 and SIP
* Qt 5.3.0 for OS X
* PyQt5 (5.3.0)
* http://www.riverbankcomputing.co.uk/software/pyqt/download5
    * `python3.4 configure.py --qmake path/to/Qt5.3.0/bin/qmake --sip /opt/local/bin/sip-3.4`
* If you want build Qt resources, we'll set /opt/local/Library/Frameworks/Python.framework/Versions/3.4/bin/ like path to $PATH evironment (this path for MacPorts)



* keyring
    * https://pypi.python.org/pypi/keyring
    * pip install keyring
* appdirs
    * pip install appdirs
* simplejson
    * pip install simplejson


## Execute

```
$ python browser.py
```

## Notes

* Cache and Configuration directory
    * /Users/username/Library/Application Support/Squardron
    * /Users/username/Library/Caches/Squardron
* Application name of keychain
    * **Squadron**

## Distribute

 $ python setup.py py2app
