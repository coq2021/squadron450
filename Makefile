

UI_FILE = ui_window.py
RC_FILE = jquery_rc.py

ALL: UI_FILE RC_FILE


UI_FILE: window.ui
	pyuic5 $< > $(UI_FILE)

RC_FILE: jquery.qrc
	pyrcc5 -o $(RC_FILE) $<
