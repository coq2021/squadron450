#!/usr/bin/env python
# -*- coding: utf-8 -*-

#############################################################################
##
## Copyright (C) 2013 Riverbank Computing Limited
## Copyright (C) 2010 Hans-Peter Jansen <hpj@urpla.net>.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
## $QT_END_LICENSE$
##
###########################################################################


from PyQt5.QtCore import Qt, QUrl, pyqtSlot, QTextStream, QIODevice, QFile, QSettings, QVariant
from PyQt5.QtWidgets import QApplication, QMainWindow, QTreeWidgetItem
from PyQt5 import QtWebKit, QtNetwork

import os
import pickle
import simplejson
import keyring
import keyring.backend

from appdirs import AppDirs

from ui_window import Ui_Window
import jquery_rc

STATE_INIT = 0
STATE_AUTO_LOGIN = 1
STATE_LOGIN = 2
STATE_START = 3
STATE_APP = 4
STATE_RUN = 5
APP_NAME = 'Squardron'
APP_AUTHOR = 'suma'

class Window(QMainWindow, Ui_Window):

    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        self.setupUi(self)
        self.state = STATE_AUTO_LOGIN
        self.appDirs = AppDirs(APP_NAME, APP_AUTHOR)

        # Load cookies
        userDataDir = self.appDirs.user_data_dir
        if not os.path.exists(userDataDir):
            os.makedirs(userDataDir)

        self.cookieStore = QSettings(userDataDir + '/cookie', QSettings.NativeFormat)
        self.cookies = QtNetwork.QNetworkCookieJar()
        self.cookies.setAllCookies([QtNetwork.QNetworkCookie.parseCookies(c)[0] for c in self.get()])
        self.settings = QSettings(userDataDir + '/config', QSettings.NativeFormat)

        self.webView.page().networkAccessManager().setCookieJar(self.cookies)

        # Disable scrollbar
        frame = self.webView.page().mainFrame()
        frame.setScrollBarPolicy(Qt.Horizontal, Qt.ScrollBarAlwaysOff)
        frame.setScrollBarPolicy(Qt.Vertical, Qt.ScrollBarAlwaysOff)

        # Enable plugin/javascript/storage
        webSetting = QtWebKit.QWebSettings.globalSettings()
        webSetting.setAttribute(QtWebKit.QWebSettings.PluginsEnabled, True)
        webSetting.setAttribute(QtWebKit.QWebSettings.DnsPrefetchEnabled, True)
        webSetting.setAttribute(QtWebKit.QWebSettings.JavascriptEnabled, True)
        webSetting.setAttribute(QtWebKit.QWebSettings.OfflineStorageDatabaseEnabled, True)
        webSetting.setAttribute(QtWebKit.QWebSettings.LocalStorageEnabled, True)

        # Set Kancolle css
        # This css apply flash layout to main window.
        webSetting.setUserStyleSheetUrl(QUrl('qrc:/overwrite.css'))

        # Prepare cache dir
        cachePath = self.appDirs.user_cache_dir

        localStoragePath = cachePath + '/LocalStorage'
        if not os.path.exists(localStoragePath):
            os.makedirs(localStoragePath)
        webSetting.setLocalStoragePath(localStoragePath)

        offlineStoragePath = cachePath + '/OfflineStorage'
        if not os.path.exists(offlineStoragePath):
            os.makedirs(offlineStoragePath)
        QtWebKit.QWebSettings.setOfflineStoragePath(offlineStoragePath)

        offlineWebApplicationCachePath = cachePath + '/OfflineWebApplication'
        if not os.path.exists(offlineWebApplicationCachePath):
            os.makedirs(offlineWebApplicationCachePath)
        QtWebKit.QWebSettings.setOfflineWebApplicationCachePath(offlineWebApplicationCachePath)

        # Prepare resource: jquery
        fd = QFile(":/jquery.min.js")

        if fd.open(QIODevice.ReadOnly | QFile.Text):
            self.jQuery = QTextStream(fd).readAll()
            fd.close()
        else:
            sys.exit(1)


    def setUrl(self, url):
        self.webView.setUrl(url)
        self.webView.page().mainFrame().javaScriptWindowObjectCleared.connect(
                self.populateJavaScriptWindowObject)

    def on_webView_loadFinished(self):
        # Begin document inspection.
        frame = self.webView.page().mainFrame()
        self.processSequence(frame)

    def processSequence(self, frame):
        # check is login process necessary
        if not ('login' in frame.baseUrl().toString()):
            self.state = STATE_START

        frame.evaluateJavaScript(self.jQuery)
        state = self.state
        if state == STATE_AUTO_LOGIN:
            print('state:autologin')

            loginForm = frame.findFirstElement('form[method=post]')
            loginId = frame.findFirstElement('#login_id')
            password = frame.findFirstElement('#password')

            if not loginForm.isNull():
                loginForm.setAttribute("onsubmit", "formExt.submit()")

            if loginId.isNull() or password.isNull():
                return

            # Load saved loginid/password from keyring
            storedLoginId = self.settings.value('login_id', None)
            storedPassword = keyring.get_password(APP_NAME, 'password')
            if (storedLoginId is None) or (storedLoginId == '') or (storedPassword is None):
                self.state = STATE_LOGIN
                return

            loginId.setAttribute('value', storedLoginId)
            password.setAttribute('value', storedPassword)

            # login automatically
            submit = frame.findFirstElement('input[type=submit]')
            if submit.isNull() or (not ('login' in frame.baseUrl().toString())):
                self.state = STATE_LOGIN
                return

            # Submit form
            self.state = STATE_START
            # We have to use 'trigger' via jQuery for DMM form which check input fields.
            # This code works but too slow!!
            #frame.evaluateJavaScript("$('form.validator').find('input[type=submit]').trigger('click')")

            # This code doesn't work. Why is this code bad?
            #submit.evaluateJavaScript('self.trigger("click")')

        elif state == STATE_START:
            print('state:start')
            #self.state = STATE_APP
            iframe = frame.findFirstElement('iframe[id=game_frame]')
            if iframe.isNull():
                return

            #pos = iframe.geometry()
            #frame.scroll(pos.x(), pos.y())
            self.state = STATE_APP

        elif state == STATE_APP:
            print('state:app')

        elif state == STATE_RUN:
            print('state:run')

    @pyqtSlot()
    def submit(self):
        frame = self.webView.page().mainFrame()
        loginId = frame.findFirstElement('#login_id')
        password =frame.findFirstElement('#password')

        # Save username/password
        idText = loginId.evaluateJavaScript('this.value')
        self.settings.setValue('login_id', idText)
        self.settings.sync()
        keyring.set_password(APP_NAME, idText, password.evaluateJavaScript('this.value'))

    def populateJavaScriptWindowObject(self):
        frame = self.webView.page().mainFrame()
        frame.addToJavaScriptWindowObject('formExt', self)

    def closeEvent(self, event):
        print('closeEvent')
        self.put([c.toRawForm().data() for c in self.cookies.allCookies()])

    def put(self, value):
        key = 'cookieJar'
        self.cookieStore.setValue(key, simplejson.dumps(value))
        self.cookieStore.sync()

    def get(self):
        key = 'cookieJar'
        v = self.cookieStore.value(key, '{}')
        if v is None:
            return None
        return simplejson.loads(v)

if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)

    # Proxy config here
    #proxy = QtNetwork.QNetworkProxy()
    #proxy.setType(QtNetwork.QNetworkProxy.HttpProxy)
    #proxy.setHostName("127.0.0.1")
    #proxy.setPort(8080)
    #QtNetwork.QNetworkProxy.setApplicationProxy(proxy)

    window = Window()
    window.show()
    #url = QUrl('/Users/suma/Dropbox/devs/kanbrowser/reversing/index.html')
    url = QUrl('http://www.dmm.com/netgame/social/-/gadgets/=/app_id=854854/')
    window.setUrl(url)

    sys.exit(app.exec_())
