#!/usr/bin/env python

from setuptools import setup

APP = ['browser.py']
RESOURCES = ['qt.conf']
OPTIONS = {
        'argv_emulation': True,
        'includes': [
            'sip', 'os',
            'keyring',
            'keyring.backends.file',
            'keyring.backends.Gnome',
            'keyring.backends.Google',
            'keyring.backends.keyczar',
            'keyring.backends.kwallet',
            'keyring.backends.multi',
            'keyring.backends.OS_X',
            'keyring.backends.pyfs',
            'keyring.backends.SecretService',
            'keyring.backends.Windows',
            'appdirs',
            #'PyQt5'
            'PyQt5._qt',
            'PyQt5.QtCore',
            'PyQt5.QtGui'
            'PyQt5.QtWidgets'
            'PyQt5.QtNetwork',
            'PyQt5.QtWebKit',
            'PyQt5.QtPrintSupport',
            ],
         'excludes': [
             'PyQt5.QtDesigner',
             'PyQt5.QtOpenGL',
             'PyQt5.QtScript', 'PyQt5.QtSql', 'PyQt5.QtTest',
             'PyQt5.QtXml', 'PyQt5.phonon',
             'PyQt5.QtQuick',
            ],
         'resources': RESOURCES
        }

setup(
    app=APP,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
)

